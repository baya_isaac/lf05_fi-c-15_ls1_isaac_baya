import java.util.Scanner;
public class Methode_A3_1 {

	public static void main(String[] args) {

	      // (E) "Eingabe"
	      // Werte für x und y festlegen:
	      // ===========================
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein :");
	      double x = scanner.nextDouble();
		System.out.println("Geben Sie eine Zahl ein :");
	      double y = scanner.nextDouble();
	      double m;
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      m = (x + y) / 2.0;
	      m = mittelwert(x,y);
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      
	      ausgabe (x, y, m);
	     // mittelwert (x, y, m);
	      
	      scanner.close();
	      
	   }
	
	public static double mittelwert ( double x, double y ) {
		
		return (x + y) / 2.0;
		
	}
	
	public static void ausgabe( double x, double y, double m ) {
		
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		
	}
	
	
	
	}