
public class Konsolenausgabe_Übung_1_Aufagbe_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.print("Das ist ein Beispielsatz."); // Ausgabe
		System.out.print("Ein Beispielsatz ist das.\n\n"); // Ausgabe
		
		System.out.println("Das ist ein \"Beispielsatz\". "); // Ausgabe mit der println, der Quasi eine Zeilen umbruch macht 
		System.out.print("Ein Beispielsatz ist das."); // Ausgabe mit eine ganz normale print gibt die ausgabe hintereinander
		
		
	}

}
